(use-modules ((geode packages encoding) #:select (soprano))
             ((gnu packages compression) #:select (unzip zip))
             ((guix build-system gnu) #:select (gnu-build-system))
             ((guix gexp) #:select (local-file))
             ((guix git-download) #:select (git-predicate))
             ((guix licenses) #:select (bsd-3))
             ((guix packages) #:select (package)))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "ProcessingLGE")
    (version "devel")
    (source
      (local-file %source-dir
                  #:recursive? #t
                  #:select? (git-predicate %source-dir)))
    (build-system gnu-build-system)
    (arguments
      '(#:phases (modify-phases %standard-phases
                   (delete 'check))))
    (propagated-inputs
     `(("soprano" ,soprano)
       ("unzip" ,unzip)
       ("zip" ,zip)))
    (home-page "https://gitlab.huma-num.fr/disco-lge/processinglge")
    (synopsis "Scripts and data for project DISCO")
    (description
      "Scripts to process La Grande Encyclopédie and suggestions of filters to
clean the ALTO files.")
    (license bsd-3)))
