#!/bin/bash

TARGET="${1%/}"

[ -d "${TARGET}" ] || { printf "Syntax: ${0##*/} TARGET_DIRECTORY\n"; exit 1; }

META="${TARGET}/meta.csv"
FILES="${TARGET}/files.csv"

printf "id,source\n" > "${META}"
printf "id,path,file\n" > "${FILES}"

find "${TARGET}/" -type f | grep -v '\(meta\|files\)\.csv' | while read FILEPATH
do
	REL_PATH="${FILEPATH##${TARGET}/}"
	DIR="${REL_PATH%/*}"
	FILENAME="${REL_PATH##*/}"
	TEXT_ID="${DIR//\//_}${FILENAME%%.*}"
	printf "${TEXT_ID},BnF\n" >> "${META}"
	printf "${TEXT_ID},${DIR},${FILENAME}\n" >> "${FILES}"
done

cd "${TARGET}"
zip -9r ../LGE.zip .
