# Processing LGE

This repository contains scripts to help process La Grande Encyclopédie as well
as data used to clean the tomes out from the OCR :

- a list of scoriæ files, one per tome, containing a list of IDs referring to
  ALTO `<String/>` elements that we marked for deletion with the use of
  [chaoui](https://gitlab.huma-num.fr/alicebrenon/chaoui) because they contain
  no relevant textual material for our study (false-positive of the OCR
  recognizing characters where there are only shapes in an engraving, arrays of
  numerical data, maths or physics formula — often poorly recognized by the OCR
  — that contain no sentences that can be parsed with tools for natural
  language)
- a set of UTF-8 symbol characters that appear in the OCRed text because of
  spots on the original pages but which doesn't occur in natural language and
  must be systematically treated as noise to filter.

Both these resources can be used by
[soprano](https://gitlab.huma-num.fr/disco-lge/soprano) and are auto-detected by
the script `LGEencode.sh` if installed properly to an available resources
directory.

## Installation

The installation process for `LGEencode.sh`is the standard UNIX way:

```bash
./configure
make
make install
```

The default installation root for `ProcessingLGE` is `/usr/local`. This can be
changed by passing the `--prefix` flag to `configure` like this

```
./configure --prefix=~/.local
```

This would install it to your user's personal home folder instead (and should
hence work without any admin privilege).
