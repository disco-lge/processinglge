#!/bin/sh

VOLUME_NUMBER="${1}"

[ -n "${VOLUME_NUMBER}" ] || { printf "Syntax: ${0##*/} VOLUME_NUMBER\n"; exit 1; }

INPUT_DIR="Vol. ${VOLUME_NUMBER}"
INPUT_ARCHIVE="${INPUT_DIR}.zip"
INPUT_ALTO="${INPUT_DIR}/ALTO"

if [ ! -d "${INPUT_DIR}" ]
then
  if [ -f "${INPUT_ARCHIVE}" ]
  then
    unzip "${INPUT_ARCHIVE}"
  else
    printf "Tome ${VOLUME_NUMBER} not found\n"
    exit 1
  fi
fi

TOME="T${VOLUME_NUMBER}"

OUTPUT_PDF="${TOME}.pdf"

[ -f "${OUTPUT_PDF}" ] || ln "${INPUT_DIR}/ARS_"* "${OUTPUT_PDF}"
[ -d "${TOME}" ] || mkdir "${TOME}"

PAGE_COUNT="$(find "${INPUT_ALTO}" -type f -name '*.xml' | wc -l)"
ONE_FILE="$(find "${INPUT_ALTO}" -type f -name '*.xml' 2> /dev/null | sed 1q)"
FILE_PREFIX="${ONE_FILE%_*}_"

for n in `seq 1 1 "${PAGE_COUNT}"`
do
  PAGE="${TOME}/p${n}.xml"
  [ -f "${PAGE}" ] || ln "${FILE_PREFIX}$(printf "%04d" "${n}").xml" "${PAGE}"
done
